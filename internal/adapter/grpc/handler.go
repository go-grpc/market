package grpc

import (
	"bus/market/internal/adapter/grpc/proto"
	"bus/market/internal/domain/market"
	"bus/market/internal/domain/market/dto"
	"context"
	"google.golang.org/grpc"
)

type IMarketUseCase interface {
	Create(ctx context.Context, dto dto.CreateMarketDto) (int64, error)
	Update(ctx context.Context, id string, dto dto.UpdateMarketDto) error
	GetOne(ctx context.Context, id string) (market.Market, error)
	Delete(ctx context.Context, id string) error
	Confirm(ctx context.Context, id string) error
}

func NewMarketServerGRPC(gServer *grpc.Server, us IMarketUseCase) {
	server := server{us}
	proto.RegisterMarketServiceServer(gServer, &server)
}

type server struct {
	us IMarketUseCase
}

func (s server) CreateMarket(ctx context.Context, request *proto.CreateMarketRequest) (*proto.CreateMarketResponse, error) {
	marketDto := dto.CreateMarketDto{
		Name:    request.Name,
		Image:   request.Image,
		OwnerId: request.OwnerId,
		Address: request.Address,
		BIN:     request.BIN,
	}
	id, err := s.us.Create(ctx, marketDto)
	var res *proto.Response
	if err != nil {
		res = &proto.Response{
			Status: 500,
			Error:  []string{"что то пошло не так"},
			Message: "ERROR",
		}
		return &proto.CreateMarketResponse{Response: res, Id: id}, nil
	}
	res = &proto.Response{
		Status: 201,
		Error:  []string{},
		Message: "CREATED",
	}
	return &proto.CreateMarketResponse{Response: res, Id: id}, nil

}

func (s server) GetOneMarket(ctx context.Context, request *proto.GetOneMarketRequest) (*proto.GetOneMarketResponse, error) {
	shop, err := s.us.GetOne(ctx, request.Id)
	var res *proto.Response
	if err != nil {
		res = &proto.Response{
			Status: 404,
			Error:  []string{"такой магазин не найден"},
			Message: "ERROR",
		}
		return &proto.GetOneMarketResponse{
			Market:   nil,
			Response: res,
		}, nil
	}
	if shop.Id == 0 {
		res = &proto.Response{
			Status: 404,
			Error:  []string{"такой магазин не найден"},
			Message: "ERROR",
		}
		return &proto.GetOneMarketResponse{
			Market:   nil,
			Response: res,
		}, nil
	}
	res = &proto.Response{
		Status: 200,
		Error:  []string{},
		Message: "OK",
	}
	return &proto.GetOneMarketResponse{
		Market:   shop.Marshal(),
		Response: res,
	}, nil
}

func (s server) UpdateMarket(ctx context.Context, request *proto.UpdateMarketRequest) (*proto.Response, error) {
	err := s.us.Update(ctx, request.Id, dto.UpdateMarketDto{
		Name:    request.Name,
		Address: request.Address,
		BIN:     request.BIN,
	})
	if err != nil {
		return &proto.Response{
			Status: 500,
			Error:  []string{"что то пошло не так"},
			Message: "ERROR",
		}, err
	}
	return &proto.Response{
		Status: 200,
		Error:  []string{},
		Message: "OK",
	}, err
}

func (s server) ConfirmMarket(ctx context.Context, request *proto.ConfirmMarketRequest) (*proto.Response, error) {
	err := s.us.Confirm(ctx, request.Id)
	if err != nil {
		return &proto.Response{
			Status: 500,
			Error:  []string{"что то пошло не так"},
			Message: "ERROR",
		}, err
	}
	return &proto.Response{
		Status: 200,
		Error:  []string{},
		Message: "OK",
	}, err
}

func (s server) DeleteMarket(ctx context.Context, request *proto.DeleteMarketRequest) (*proto.Response, error) {
	err := s.us.Delete(ctx, request.Id)
	if err != nil {
		return &proto.Response{
			Status: 500,
			Error:  []string{"что то пошло не так"},
			Message: "ERROR",
		}, err
	}
	return &proto.Response{
		Status: 200,
		Error:  []string{},
		Message: "OK",
	}, err
}
