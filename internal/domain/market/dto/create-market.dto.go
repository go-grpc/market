package dto

type CreateMarketDto struct {
	Name string `json:"name"`
	Image string `json:"image"`
	OwnerId uint32 `json:"owner_id"`
	Address string `json:"address"`
	BIN string `json:"bin"`
}
