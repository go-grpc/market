package dto

type UpdateMarketDto struct {
	Name string `json:"name"`
	Address string `json:"address"`
	BIN string `json:"bin"`
}