package market

import "bus/market/internal/adapter/grpc/proto"

type Market struct {
	Id       uint32 `json:"id"`
	Name     string `json:"name"`
	Image    string `json:"image"`
	OwnerId  uint32 `json:"owner_id"`
	Address  string `json:"address"`
	IsActive bool   `json:"is_active"`
	IsBlock  bool   `json:"is_block"`
	BIN      string `json:"bin"`
}

func (m *Market) Marshal() *proto.Market {
	return &proto.Market{
		Id:       m.Id,
		Name:     m.Name,
		Image:    m.Image,
		Address:  m.Address,
		OwnerId:  m.OwnerId,
		BIN:      m.BIN,
		IsActive: m.IsActive,
		IsBlock:  m.IsBlock,
	}
}
