package repository

import (
	"bus/market/internal/domain/market"
	"bus/market/internal/domain/market/dto"
	"bus/market/internal/domain/market/usecase"
	"bus/market/pkg/client/postgresql"
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
)

type marketRepository struct {
	client postgresql.Client
}

func (m marketRepository) GetOneByName(ctx context.Context, name string) (int, error) {
	var id int
	q := `SELECT id FROM market WHERE name = $1`
	rows, err := m.client.Query(ctx, q, name)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, err

}

func (m marketRepository) Save(ctx context.Context, dto dto.CreateMarketDto) (int64, error) {
	var id int64
	q := `INSERT INTO market (name, image, address, bin, owner_id) VALUES ($1,$2,$3,$4,$5) RETURNING id`
	rows, err := m.client.Query(ctx, q, dto.Name, dto.Image, dto.Address, dto.BIN, dto.OwnerId)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, err
}

func (m marketRepository) GetOne(ctx context.Context, id string) (market.Market, error) {
	var shop market.Market
	q := `SELECT id,name,owner_id,address,bin,is_active,is_block,image FROM market WHERE id = $1`
	rows, err := m.client.Query(ctx, q, id)
	if err != nil {
		if err == pgx.ErrNoRows {
			return shop, errors.New("такого магазина нету")
		}
		return shop, err
	}
	for rows.Next() {
		if err = rows.Scan(&shop.Id, &shop.Name, &shop.OwnerId, &shop.Address,
			&shop.BIN, &shop.IsActive, &shop.IsBlock, &shop.Image); err != nil {
			return market.Market{}, err
		}
	}
	return shop, err
}

func (m marketRepository) Delete(ctx context.Context, id string) error {
	q := `DELETE FROM market WHERE id = $1`
	_, err := m.client.Query(ctx, q, id)
	if err != nil {
		return err
	}
	return nil
}

func (m marketRepository) Update(ctx context.Context, id string, dto dto.UpdateMarketDto) error {
	q := `UPDATE market SET name = $1 , address = $2 , bin = $3 WHERE id = $4`
	_, err := m.client.Query(ctx, q, dto.Name, dto.Address, dto.BIN, id)
	if err != nil {
		return err
	}
	return nil
}

func (m marketRepository) Confirm(ctx context.Context, id string) error {
	q := `UPDATE market SET is_active = true WHERE id = $1`
	_, err := m.client.Query(ctx, q, id)
	if err != nil {
		return err
	}
	return nil
}

func NewMarketRepository(client postgresql.Client) usecase.IMarketRepository {
	return marketRepository{client}
}
