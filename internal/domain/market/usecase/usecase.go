package usecase

import (
	"bus/market/internal/adapter/grpc"
	"bus/market/internal/domain/market"
	"bus/market/internal/domain/market/dto"
	"context"
	"errors"
)

type IMarketRepository interface {
	Save(ctx context.Context, dto dto.CreateMarketDto) (int64, error)
	GetOne(ctx context.Context, id string) (market.Market, error)
	GetOneByName(ctx context.Context, name string) (int, error)
	Delete(ctx context.Context, id string) error
	Update(ctx context.Context, id string, dto dto.UpdateMarketDto) error
	Confirm(ctx context.Context, id string) error
}

type MarketUseCase struct {
	r IMarketRepository
}

func (m MarketUseCase) Create(ctx context.Context, dto dto.CreateMarketDto) (int64, error) {
	candidate, err := m.r.GetOneByName(ctx, dto.Name)
	if err != nil {
		return 0, err
	}
	if candidate != 0 {
		return 0, errors.New("магазин с таким названием есть")
	}
	id, err := m.r.Save(ctx, dto)
	if err != nil {
		return 0, err
	}
	return id, err
}

func (m MarketUseCase) Update(ctx context.Context, id string, dto dto.UpdateMarketDto) error {
	err := m.r.Update(ctx, id, dto)
	if err != nil {
		return err
	}
	return nil
}

func (m MarketUseCase) GetOne(ctx context.Context, id string) (market.Market, error) {
	shop, err := m.r.GetOne(ctx, id)
	if err != nil {
		return market.Market{}, err
	}
	return shop, err
}

func (m MarketUseCase) Delete(ctx context.Context, id string) error {
	err := m.r.Delete(ctx,id)
	if err !=nil{
		return err
	}
	return nil
}

func (m MarketUseCase) Confirm(ctx context.Context, id string) error {
	err := m.r.Confirm(ctx,id)
	if err !=nil{
		return err
	}
	return nil
}

func NewMarketUseCase(r IMarketRepository) grpc.IMarketUseCase {
	return MarketUseCase{r}
}
