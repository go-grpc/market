CREATE TABLE market(
    id SERIAL PRIMARY KEY,
    name varchar(144) unique not null,
    image varchar(255) not null ,
    address varchar(255) not null ,
    is_active bool default false,
    is_block bool default false,
    bin varchar(255) not null ,
    owner_id int not null
)