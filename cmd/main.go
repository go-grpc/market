package main

import (
	grpc2 "bus/market/internal/adapter/grpc"
	"bus/market/internal/domain/market/repository"
	"bus/market/internal/domain/market/usecase"
	"bus/market/pkg/client/postgresql"
	"context"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}
	client, err := postgresql.NewClient(context.Background(), 5, os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_DB"))
	if err != nil {
		panic(err)
	}
	listener, err := net.Listen("tcp", ":"+os.Getenv("PORT"))
	marketRepository := repository.NewMarketRepository(client)
	marketUseCase := usecase.NewMarketUseCase(marketRepository)

	srv := grpc.NewServer()
	grpc2.NewMarketServerGRPC(srv, marketUseCase)

	reflection.Register(srv)
	if err := srv.Serve(listener); err != nil {
		panic(err)
	}
	println("server started")
}
